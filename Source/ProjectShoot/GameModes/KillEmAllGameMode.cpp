#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "GameFramework/Controller.h"
#include "ProjectShoot/Controllers/ShooterAIController.h"

void AKillEmAllGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);
	
	APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());

	if (PlayerController != nullptr)
	{
		EndGame(false);
		//PlayerController->GameHasEnded(nullptr, false);
	}
	for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!AIController->IsDead()) { return; }
	}
	EndGame(true);
}
void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
		UE_LOG(LogTemp, Warning, TEXT("Player win == 1, Player lose == 0: %d"), bIsWinner);
		Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
	}
}
