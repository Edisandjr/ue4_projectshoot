

#pragma once

#include "CoreMinimal.h"
#include "ProjectShoot/GameModes/ProjectShootGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTSHOOT_API AKillEmAllGameMode : public AProjectShootGameModeBase
{
	GENERATED_BODY()
public:
	virtual void PawnKilled(APawn* PawnKilled) override;
private:
	void EndGame(bool bIsPlayerWinner);

};
